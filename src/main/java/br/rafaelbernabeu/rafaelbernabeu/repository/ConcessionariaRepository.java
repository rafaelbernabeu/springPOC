package br.rafaelbernabeu.rafaelbernabeu.repository;

import br.rafaelbernabeu.rafaelbernabeu.entity.Concessionaria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConcessionariaRepository extends JpaRepository<Concessionaria, Long> {


}
