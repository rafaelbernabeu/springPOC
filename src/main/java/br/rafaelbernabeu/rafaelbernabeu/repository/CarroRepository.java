package br.rafaelbernabeu.rafaelbernabeu.repository;

import br.rafaelbernabeu.rafaelbernabeu.entity.Carro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarroRepository extends JpaRepository<Carro, Long> {


}
